# Training plan API

In germany we have the problem, that some data is public available but not direct usable for machines.
So if you need that data you have to write first a scrapper.
There is a movement to write APIs for doing this, have a look at [bund.dev](https://bund.dev)
Actually I want to write an App for automated checks for trainings with last minute places.
To do so I will first write an API so everyone can use that data.

## Model

### Difference between training and course
Course is only the specification but not scheduled.
The training contains a course and a schedule.
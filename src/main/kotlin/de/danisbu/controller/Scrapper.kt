package de.danisbu.controller

import org.slf4j.LoggerFactory
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

class Scrapper (val uri: String = "https://www.thw-ausbildungszentrum.de/THW-BuS/DE/Ausbildungsangebot/Lehrgangskalender/lehrgangskalender_node.html") {
    private val logger = LoggerFactory.getLogger(Scrapper::class.java)

    fun scrap(): String {
        val client = HttpClient.newBuilder().build()
        val request = HttpRequest.newBuilder()
            .uri(URI.create(uri))
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString())

        logger.info(response.toString())

        return response.body()
    }
}
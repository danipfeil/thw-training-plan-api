package de.danisbu.modell

data class Course(
    val title: Title,
    val type: Type,
    val id: CourseId,
)
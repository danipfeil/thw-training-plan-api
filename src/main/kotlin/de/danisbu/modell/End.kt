package de.danisbu.modell

import java.time.LocalDateTime

data class End (val value: LocalDateTime)

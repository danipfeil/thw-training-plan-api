package de.danisbu.modell

import java.time.LocalDateTime

data class RegistrationDeadline (val value: LocalDateTime)

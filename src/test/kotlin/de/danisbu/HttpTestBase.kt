package de.danisbu

import org.mockserver.client.MockServerClient
import org.mockserver.integration.ClientAndServer
import kotlin.test.AfterTest
import kotlin.test.BeforeTest

open class HttpTestBase {
    private val port = 8081
    private val host = "localhost"
    val uri = "http://$host:$port"
    var mockServer: MockServerClient = MockServerClient(host, port)

    @BeforeTest
    fun prepare() {
        mockServer = ClientAndServer.startClientAndServer(port)
    }

    @AfterTest
    fun tearDown() {
        mockServer.close()
    }
}
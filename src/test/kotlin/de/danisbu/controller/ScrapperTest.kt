package de.danisbu.controller

import de.danisbu.HttpTestBase
import org.junit.Test
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import kotlin.test.assertEquals

class ScrapperTest: HttpTestBase() {
    val path = "/THW-BuS/DE/Ausbildungsangebot/Lehrgangskalender/lehrgangskalender_node.html"

    @Test
    fun scrap(){
        val content = "Hello World"

        mockServer.`when`(
            request()
                .withMethod("GET")
                .withPath(path)
        ).respond(
            response()
                .withStatusCode(200)
                .withBody(content)
        )

        val scrapedData = Scrapper("$uri$path").scrap()

        assertEquals(content, scrapedData)
    }
}